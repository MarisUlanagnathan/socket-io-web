import React, { Component } from "react";
import "react-table-v6/react-table.css";
import ReactTable from "react-table-v6";
import { connect } from "react-redux";
import openSocket from "socket.io-client";
import store from "./store";
import * as Actions from "./action";
export class App extends Component {
  constructor(props) {
    super(props);
    this.socket = openSocket('https://apstaging.matrixbscloud.com/apis//auditor/v1/canon/getStores?templateId=5fc0efaed524acc508922941');
    this.socket.on(Actions.GET_SUCCESS_PEOPLE, data => {
      store.dispatch(Actions.getSuccessPeople(data));
    });
    this.socket.on(Actions.GET_FAILURE_PEOPLE, err => {
      store.dispatch(Actions.getFailurePeople(err));
    });
  }
  componentDidMount() {
    // Accessible because we 'connected'    
    this.props.getPeople({ socket: this.socket }); 
  }

  render() {
    const columns = [
      {
        Header: "Name",
        accessor: "name" // String-based value accessors!
      },
      {
        Header: "Status",
        accessor: "age"
      }
    ];
    const data = [
      {
        name: "maris",
        age: "pending"
      },
      {
        name: "sundar",
        age: "completed"
      },
      {
        name: "raj",
        age: "inProgress"
      },
      {
        name: "sethu",
        age: "pending"
      }
    ];
    return (
      <div style={{ flexGrow: 1, display: "flex", flexFlow: "column" }}>
        <ReactTable data={data} columns={columns} />
      </div>
    );
  }
}

export default connect(state => state, { getPeople: Actions.getPeople })(App);
