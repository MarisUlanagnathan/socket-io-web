import * as Actions from  './action'; // Import our actions
const initialState = {
  people: {},
  isFetching: true, // Default to fetching..
  error: null
};
export default (state = initialState, action) => {
  switch (action.type) {
    case Actions.GET_REQUEST_PEOPLE:
      return {
        ...state,
        isFetching: true,
        error: null
      };
    case Actions.GET_SUCCESS_PEOPLE:
      return {
        ...state,
        people: action.data.results,
        isFetching: false
      };
    case Actions.GET_FAILURE_PEOPLE:
      console.log('Error: ', action.error);
      return {
        ...state,
        error: action.error,
        isFetching: false
      };
    default:
      return state;
  }
};
