export const GET_REQUEST_PEOPLE = 'GET_REQUEST_PEOPLE';
export const GET_SUCCESS_PEOPLE = 'GET_SUCCESS_PEOPLE';
export const GET_FAILURE_PEOPLE = 'GET_FAILURE_PEOPLE';
// Action creators return a consistent "command" object.
export const getRequestPeople = () => ({
  type: GET_REQUEST_PEOPLE
});
export const getSuccessPeople = data => {
  return {
    type: GET_SUCCESS_PEOPLE,
    data
  };
};
export const getFailurePeople = err => ({
  type: GET_FAILURE_PEOPLE,
  err
});
// This method has one required option which is the reference to
// socket being utilized.  That connection happens somewhere else.
// Assume we're connected when this function is called.
export const getPeople = options => async dispatch => {
  // Notify our state that we're doing something asynchronous.     
  dispatch(getRequestPeople());  
  // Sanity, don't need to pass the socket itself down the wire.
  const { socket } = options;
  delete options.socket;
try {
    // Emit the request to the back-end via the socket.
    socket.emit(GET_REQUEST_PEOPLE, options);
   } catch (err) {
    dispatch(getFailurePeople(err));
   }
};